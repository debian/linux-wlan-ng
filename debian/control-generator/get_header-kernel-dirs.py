#!/usr/bin/env python
# This is a modified version of gencontrol.py of the Debian kernel team
# Thanks to the Debian Kernel Team for their work.
# This program prints the 'kernel-header-dirs' var for an arch. Or
# prints all dirs if no arch is given.
# It need the proper linux-header-2.6.X package installed. There is
# a soft link arch point to /usr/linux-header-2.6.X/debian/arch
# and a link to /usr/linux-header-2.6.X-X/debian/changelog
#
# Copyright by
# Debian Kernel Team <debian-kernel@lists.debian.org> 
# Victor Seva Lopez <linuxmaniac@torreviejawireless.org>
# License:
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License with
# the Debian GNU/Linux distribution in file /usr/share/common-licenses/GPL;
# if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA  02111-1307  USA
#
# See /usr/share/common-licenses/GPL for the full text of the GPL.
import os, os.path, re, sys, textwrap, ConfigParser
sys.path.append("lib")
from debian_linux import *

def main():
    c = config()

    if len(sys.argv) == 2 :
      arch = sys.argv[1]
      if c.has_key(arch):
        print c[arch]['kernel-header-dirs']
    elif len(sys.argv) == 1:
      for arch in iter(c['base']['arches']):
        config_entry = c[arch,]
        if config_entry.get('available', True):
          print arch, c[arch]['kernel-header-dirs']
    else:
      print "Just one parameter please"

if __name__ == '__main__':
    main()
