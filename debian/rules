#!/usr/bin/make -f

# debug
#export DH_VERBOSE=y
#export TO_NULL=&>/dev/null
#TO_NULL=&>/dev/null
#MAKEFLAGS+=-s --no-print-directory

# Hardening
DPKG_EXPORT_BUILDFLAGS = 1
include /usr/share/dpkg/buildflags.mk

# used by debian/config-for-lwng that is sourced by ./Configure
# in this way no modules are built and no errors are reported if there are no
# kenrel headers
ENV=BUILD_MODULES=n BUILD_UTILS=y

#export DH_OPTIONS

# some shortcuts
PWD=$(shell pwd)
LWNTMP=.modules
LWNS=linux-wlan-ng

WARN_MSG="hex files found! please remove them from the source package"
SRC_TBALL_MSG="building source tarball"
INDEP_DEB_MSG="building deb for -source and doc"
UTILS_DEB_MSG="building deb for utilities"
UTILS_BUILD_MSG="building utilities"
FIRMWARE_DEB_MSG="building deb firmware"
NO_DEB_MSG="no debs id dir "

#if not set it may be 'cc' and on hppa the cross compiler is named
# blablabla-gcc, not blablabla-cc
CC := gcc

###################### CONFIGURATION ###################################
configure-stamp:
	@echo detected flavours for current arch are: $(flavours)
	# to make the configure script happy we have to provide a real
	# kernel source tree.
	$(ENV) CC="$(CC)" ./Configure -d debian/config-for-lwng $(TO_NULL)
	touch configure-stamp

###################### INDEP PART ######################################

# creates the files for the tarball
build-indep: build-stamp-indep
build-stamp-indep: configure-stamp
	printf "\n\n\n***** $(SRC_TBALL_MSG) *****\n\n\n"
	mkdir -p $(LWNTMP)/modules/$(LWNS)
	cp -ra * $(LWNTMP)/modules/$(LWNS)
	make -C $(LWNTMP)/modules/$(LWNS) clean
	rm -rf $(LWNTMP)/modules/$(LWNS)/debian
	rm -rf $(LWNTMP)/modules/$(LWNS)/*-stamp
	mkdir -p $(LWNTMP)/modules/$(LWNS)/debian/
	cp debian/changelog debian/compat debian/copyright debian/config-for-lwng \
		$(LWNTMP)/modules/$(LWNS)/debian
	cp debian/*.modules.in \
		$(LWNTMP)/modules/$(LWNS)/debian
	install -m755 debian/rules.modules \
		$(LWNTMP)/modules/$(LWNS)/debian/rules
# fail if firmware files found: THEY SHOULD NOT BE THERE!!!!
	if [ -n "`find -name '*.hex'`" ]; then \
		printf "\n\n\n***** $(WARN_MSG) *****\n\n\n"; \
		exit 1; \
	fi
#	rm -f $(LWNTMP)/modules/$(LWNS)/src/prism2/*.hex
	touch build-stamp-indep

# creates the tarball
install-stamp-indep: build-stamp-indep
	cd $(LWNTMP);\
		tar cjf ../$(LWNS).tar.bz2 modules
# install the firmware stuff
	TGT=debian/tmp/usr/share/linux-wlan-ng-firmware/debian/;\
	mkdir -p $$TGT;\
	mkdir -p debian/tmp/usr/bin/;\
	mkdir -p debian/tmp/usr/share/doc/linux-wlan-ng-firmware/;\
	cp debian/firmware.install.in \
		$$TGT/linux-wlan-ng-firmware-files.install;\
	cp debian/firmware.dirs.in \
		$$TGT/linux-wlan-ng-firmware-files.dirs;\
	cp debian/changelog \
		$$TGT;\
	cp debian/control.firmware \
		$$TGT/control;\
	cp debian/rules.firmware \
		$$TGT/rules;\
	chmod +x $$TGT/rules; \
	cp debian/config-for-lwng \
		$$TGT;\
	echo 7 > $$TGT/compat;\
	V=`dpkg-parsechangelog | grep ^Version | cut -f 2 -d : | tr -d " "`;\
	  sed s/@@VERSION@@/$$V/ debian/linux-wlan-ng-build-firmware-deb >\
	  debian/tmp/usr/bin/linux-wlan-ng-build-firmware-deb; \
	  sed s/@@VERSION@@/$$V/ debian/linux-wlan-ng-build-firmware-deb.1 >\
	  debian/tmp/usr/bin/linux-wlan-ng-build-firmware-deb.1;
	chmod 0755 debian/tmp/usr/bin/linux-wlan-ng-build-firmware-deb
	cp debian/linux-wlan-ng-firmware.copyright \
	  debian/tmp/usr/share/doc/linux-wlan-ng-firmware/
	touch install-stamp-indep

binary-indep: DH_OPTIONS=-i
binary-indep: install-stamp-indep
	printf "\n\n\n***** $(INDEP_DEB_MSG) *****\n\n\n"
	dh_install $(DH_OPTIONS) -Xrc.d
	dh_installchangelogs $(DH_OPTIONS) CHANGES
	dh_installdocs  $(DH_OPTIONS)
	dh_installexamples $(DH_OPTIONS)
	dh_link $(DH_OPTIONS)
	dh_fixperms $(DH_OPTIONS)
	dh_installdeb  $(DH_OPTIONS)
	dh_gencontrol $(DH_OPTIONS)
	dh_compress $(DH_OPTIONS)
	dh_md5sums $(DH_OPTIONS)
	dh_builddeb $(DH_OPTIONS)

####################### ARCH PART ######################################

# install stuff that is arch dependent
install-stamp: DH_OPTIONS=-plinux-wlan-ng -pprism2-usb-firmware-installer
install-stamp: build-stamp
	printf "\n\n\n***** $(UTILS_DEB_MSG) *****\n\n\n"

	chmod +x debian/linux-wlan-ng-post-down debian/linux-wlan-ng-pre-up \
		debian/linux-wlan-ng-resume debian/linux-wlan-ng-suspend
	dh_testdir $(DH_OPTIONS)
	dh_testroot $(DH_OPTIONS)
	dh_prep $(DH_OPTIONS)
	dh_installdirs $(DH_OPTIONS)
	
# install utils and docs
	mkdir -p debian/tmp/etc/hotplug/	
	$(MAKE) install $(TO_NULL)
	cp src/prism2/*pda debian/tmp/etc/wlan/
	mkdir -p debian/tmp/usr/bin
	cp srec2fw debian/tmp/usr/bin/srec2fw
	touch install-stamp


# build utils and modules (all arch stuff)
build-arch: build-stamp
build-stamp: configure-stamp
	dh_testdir
	printf "\n\n\n***** $(UTILS_BUILD_MSG) *****\n\n\n"
	$(MAKE) all $(TO_NULL)
	$(CC) -o srec2fw debian/srec2fw.c

	touch build-stamp

# package all arch dependent stuff
binary-arch: DH_OPTIONS=-plinux-wlan-ng -pprism2-usb-firmware-installer
binary-arch: install-stamp
	dh_install $(DH_OPTIONS)
	dh_installchangelogs $(DH_OPTIONS) CHANGES
	dh_installdocs  $(DH_OPTIONS)
	dh_installexamples $(DH_OPTIONS)
	dh_strip  $(DH_OPTIONS)
	dh_link $(DH_OPTIONS)
	dh_fixperms  $(DH_OPTIONS)
	dh_shlibdeps  $(DH_OPTIONS)
	dh_installdeb  $(DH_OPTIONS)
	chmod a-x debian/linux-wlan-ng/etc/pcmcia/wlan-ng.*
	chmod a-x debian/linux-wlan-ng/etc/wlan/wlancfg-DEFAULT
	chmod a+x debian/linux-wlan-ng/etc/wlan/shared
	dh_installman  $(DH_OPTIONS) man/*.1
	dh_installman  -pprism2-usb-firmware-installer debian/srec2fw.1
	dh_gencontrol  $(DH_OPTIONS)
	dh_compress    $(DH_OPTIONS)
	dh_md5sums     $(DH_OPTIONS)
	dh_builddeb    $(DH_OPTIONS)

build: build-stamp

############################ CLEAN ################################

clean: configure-stamp clean1
clean1:
	dh_testdir
	dh_testroot
	$(MAKE) mrproper $(TO_NULL)
	$(MAKE) -C src/prism2/download clean $(TO_NULL)
	$(MAKE) -C debian/control-generator clean $(TO_NULL)
	dh_clean debian/postinst

# stamps
	$(RM) configure-stamp build-stamp install-stamp \
		build-stamp-indep install-stamp-indep \
		install-firmware-stamp
	
# temporary stuff
	rm -rf $(LWNTMP) $(LWNS).tar.bz2
	
# firmware
	rm -rf .firmware

########################## MAIN ##################################

binary: binary-indep binary-arch

.PHONY: build build-arch build-indep clean binary-indep binary-arch binary
